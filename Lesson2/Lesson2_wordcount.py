# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# +++ваш код+++
# Определите и заполните функции print_words(filename) и print_top(filename).
# Вы также можете написать вспомогательную функцию, которая читает файл,
# строит по нему словарь слово/количество и возвращает этот словарь.
# Затем print_words() и print_top() смогут просто вызывать эту вспомогательную функцию.

import codecs
import sys
reload(sys)
sys.setdefaultencoding('utf-8')


def get_dict(filename):
    f = codecs.open(filename)
    word_list = f.read().split()

    word_dict = {}
    for w in word_list:
        w = w.strip('.,`!?()\';:').lower()
        if w:
            word_dict[w] = word_dict.get(w, 0) + 1

    return word_dict


def print_words(filename):
    word_dict = get_dict(filename)

    for key in sorted(word_dict.keys()):
        print '{0:20} {1:5}'.format(key, word_dict[key])


def print_top(filename):
    word_dict = get_dict(filename)

    for k, v in sorted(word_dict.items(), key=lambda x: x[1], reverse=True)[:20]:
        print '{0:20} {1:5}'.format(k, v)


def main():
    if len(sys.argv) != 3:
        print('usage: python wordcount.py {--count | --topcount} file')
        sys.exit(1)

    option = sys.argv[1]
    filename = sys.argv[2]
    if option == '--count':
        print_words(filename)
    elif option == '--topcount':
        print_top(filename)
    else:
        print('unknown option: ' + option)
    sys.exit(1)


if __name__ == '__main__':
    main()
