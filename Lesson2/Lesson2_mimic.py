# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import random
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import codecs


def mimic_dict(filename):
    """Возвращает имитационный словарь, сопоставляющий каждое слово
    со списом слов, которые непосредственно следуют за ним в тексте"""
    f = codecs.open(filename)
    word_list = f.read().split()

    res = {}
    elem = ''

    for x in word_list:
        if elem in res:
            res[elem].append(x)
        else:
            res[elem] = [x, ]
        elem = x
    return res


def print_mimic(mimic_dict, word):
    """Принимает в качестве аргументов имитационный словарь и начальное слово,
        выводит 200 случайных слов."""
    res = []

    for i in xrange(200):
        if word not in mimic_dict:
            word = ''
        word = random.choice(mimic_dict[word])
        res.append(word)

    print ' '.join(res)


def main():
    if len(sys.argv) != 2:
        print('usage: ./mimic.py file-to-read')
        sys.exit(1)

    d = mimic_dict(sys.argv[1])
    print_mimic(d, '')


if __name__ == '__main__':
    main()
