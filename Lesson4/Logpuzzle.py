#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
import re
import sys
import urllib


def read_urls(filename):
    """ 
    Возвращает список url изображений из данного лог файла,
    извлекая имя хоста из имени файла (apple-cat.ru_access.log). Вычищает
    дубликаты и возвращает список url, отсортированный по названию изображения.
    """

    f = open(filename, 'r')
    s = f.read()
    groups = re.findall(r'GET\s+(/images/animals_\d+.jpg)', s)

    host = re.search(r'(.*)_access\.log', filename).group(1)

    return sorted(['http://' + host + x for x in set(groups)])


def download_images(img_urls, dest_dir):
    """
    Получает уже отсортированный спискок url, скачивает каждое изображение
    в директорию dest_dir. Переименовывает изображения в img0.jpg, img1.jpg и тд.
    Создает файл index.html в заданной директории с тегами img, чтобы 
    отобразить картинку в сборе. Создает директорию, если это необходимо.
    """

    if not os.path.exists(dest_dir):
        os.makedirs(dest_dir)
    if not os.path.exists(os.path.join(dest_dir, 'img')):
        os.makedirs(os.path.join(dest_dir, 'img'))

    html = ['<html><body>']
    for i, url in enumerate(img_urls):
        filename = os.path.join('img', 'img{}.jpg'.format(i))
        urllib.urlretrieve(url, os.path.join(dest_dir, filename))
        html.append('<img src="{}">\n'.format(filename))
    html.append('</body></html>')

    f = open(os.path.join(dest_dir, 'index.html'), 'w')
    f.write(''.join(html))
    f.close()


def main():
    args = sys.argv[1:]

    if not args:
        print('usage: [--todir dir] logfile')
        sys.exit(1)

    todir = ''
    if args[0] == '--todir':
        todir = args[1]
        del args[0:2]

    img_urls = read_urls(args[0])
    print img_urls

    if todir:
        download_images(img_urls, todir)
    else:
        print('\n'.join(img_urls))


if __name__ == '__main__':
    main()
