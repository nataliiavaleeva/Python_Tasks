# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import sys
import codecs
import re


def extract_names(filename):
    f = codecs.open(filename, 'r', encoding='utf8')

    groups = re.findall(r"""
            <tr.*?>\s*
                <td.*?>\s*
                    (.*?)
                </td>\s*
                <td.*?>\s*
                    (.*?)
                </td>\s*
                <td.*?>\s*
                    (.*?)
                </td>\s*
                <td.*?>\s*
                    (.*?)
                </td>\s*
                <td.*?>\s*
                    (.*?)
                </td>\s*
                <td.*?>\s*
                    (.*?)
                </td>\s*
                <td.*?>\s*
                    (.*?)
                </td>\s*
            </tr>
 """, f.read(), re.DOTALL | re.VERBOSE)

    res = dict([(x[1].strip(), [y.strip() for y in x[2:]]) for x in groups])

    return res


def print_names(babynames):
    # +++ваш код+++
    years = ['2012', '2010', '2005', '2000', '1990']

    while True:
        user_input = raw_input("Enter year in '2012','2010','2005','2000','1990'\nor exit for exit\n")
        if user_input == 'exit':
            break
        elif user_input not in years:
            continue

        year_index = years.index(user_input)

        for k, v in sorted(babynames.items(), key=lambda x: x[0]):
            print '{0:20}\t{1:10}'.format(k, v[year_index])


def main():
    # Код разбора командной строки
    # Получим список аргументов командной строки, отбросив [0] элемент,
    # который содержит имя скрипта
    args = sys.argv[1:]

    if not args:
        print('usage: filename')
        sys.exit(1)

    filename = args[0]
    babynames = extract_names(filename)
    print_names(babynames)


if __name__ == '__main__':
    main()
